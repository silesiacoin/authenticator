const SocketIo = require('./socket').socketIo;
const express = require('express');
const Web3 = require('web3');
const net = require('net');
const Accounts = require('web3-eth-accounts');
const cors = require('cors');
const uport = require('uport-credentials');
const app = express();
const { verifyJWT } = require('did-jwt');
const bodyParser = require('body-parser');
const qrCode = require('qrcode');
const port = (process.env.port || 8081);
const io = require('socket.io');
const ioRedis = require('socket.io-redis');
const appName = (process.env.APP_NAME || 'ssc-authenticator');
const ipcLocator = (process.env.NODE_ADDRESS || '/root/.silesiacoin/ssc.ipc');


// uPort setup
const { did, privateKey } = uport.Credentials.createIdentity();
const credentials = new uport.Credentials({
    appName: appName,
    did,
    privateKey
});

// Dependency injection
const socket = SocketIo(
    io,
    ioRedis,
    credentials,
    qrCode,
    (process.env.socketPort || 8083)
);

// Socket.io
const activeSocket = socket.attachListeners();

app.set('redisHost', (process.env.REDIS_HOST || 'redis'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.options('/accounts', cors());
app.use(cors());

app.get('/', (req, res) => res.json(res.json(indexResponse())));
app.get('/accounts/:address', async(req, res) => res.json(await checkBalance(req)));
app.post('/accounts/verify/:connectionId', async(req, res) => res.json(await verifyDisclosureResponse(req)));
app.put('/accounts', async(req, res) => res.json(await createAccount()));

app.listen(port, () => console.log(`Authenticator listening on port ${port}!`));

const web3 = new Web3(new Web3.providers.IpcProvider(ipcLocator, net));

const indexResponse = () => {
    return {
        routes: app._router.stack
            .filter(possibleRoute => possibleRoute.route)
            .map(possibleRoute => {
                return {
                    route: possibleRoute.route.path,
                    methods: possibleRoute.route.methods
                }
            })
    }
};

const createAccount = async() => {
    const accounts = new Accounts();
    const { address, privateKey } = accounts.create();

    return {
        address,
        privateKey
    }
};

const checkBalance = async(req) => {
    const { address } = req.params;

    try {
        const balance = await web3.eth.getBalance(address);
        const sscConverted = web3.utils.fromWei(balance);

        return {
            accountNumber: address,
            balance: {
                wei: balance,
                ssc: sscConverted
            }
        }
    } catch (error) {
        return {
            error: error.message
        }
    }
};

const verifyDisclosureResponse = async(req) => {
    try {
        const { token } = req.body;
        const { connectionId } = req.params;
        const response = await verifyJWT(token, { audience: credentials.did });
        const profile = await credentials.processDisclosurePayload(response);
        profile.publicEncKey = profile.boxPub;

        const nsp = activeSocket.of('/');
        nsp.to(connectionId).emit('disclosureAcceptance', JSON.stringify({
            profile: profile
        }));

        return {
            profile: profile
        }
    } catch (error) {
        return {
            error: error.message
        }
    }
};
