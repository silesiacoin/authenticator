@api @createAccount
Feature: As an api a user
  I must be able to check balance

  @Creating
  Scenario: Create account in blockchain
    When send put request to "http://127.0.0.1:8081/accounts", with data
    """
    {}
    """
    Then JSON node "address" should exist
    Then JSON node "privateKey" should exist