const { When, Then } = require('cucumber');
const got = require('got');
const assert = require('assert');

When(/^send ([^"]*) request to "([^"]*)", with data$/, async function (method, url, docString) {
    const body = JSON.parse(docString);
    let data = {};
    method = method.toLowerCase();

    const stringBody = JSON.stringify(body);

    if ('get' !== method) {
        data = {
            headers: { 'Content-Type': 'application/json' },
            body: stringBody
        };
    }

    this.response = await got[method](url, data);
});

Then(/^I send ([^"]*) request to "([^"]*)", with token from last authenticator response$/, async function (method, url) {
    const body = JSON.parse(this.response.body);
    let data = {};
    method = method.toLowerCase();

    const tokenJsonRequest = JSON.parse('{ "token": "' + body.disclosureRequest + '" }');
    const stringBody = JSON.stringify(tokenJsonRequest);

    if ('get' !== method) {
        data = {
            headers: { 'Content-Type': 'application/json' },
            body: stringBody
        };
    }

    this.response = await got[method](url, data);
});

Then(/^JSON node "([^"]*)" should exist$/, async function (node) {
    const body = JSON.parse(this.response.body);
    assert.equal(true, body.hasOwnProperty(node));
});

Then(/^JSON node "([^"]*)" should have data$/, async function (node, docString) {
    const body = JSON.parse(this.response.body);
    assert.equal(true, body.hasOwnProperty(node));
    const nodeFromBody = body[node];
    const assertion = JSON.parse(docString.toString());

    Object.keys(assertion).map((key) => {
        if (assertion[key] === '{STRING}') {
            assert.equal(typeof assertion[key], 'string');

            return;
        }

        assert.equalValues(assertion[key], nodeFromBody[key]);
    });
});

assert.equalValues = (value1, value2) => {
    try {
        assert.strictEqual(value1, value2);
    } catch (error) {
        if (error.message.indexOf('objects identical but not reference equal') > -1) {
            return;
        }

        throw error;
    }
};