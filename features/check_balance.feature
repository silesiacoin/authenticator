@api @CheckBalance
Feature: As an api a user
  I must be able to check balance

  @Check
  Scenario: Checking balance with valid key
    When send get request to "http://127.0.0.1:8081/accounts/0x4eBcd101cbFae4b72c070eBE569B8dEb5436FBbC", with data
    """
    {}
    """
    Then JSON node "accountNumber" should exist
    Then JSON node "balance" should have data
    """
     { "wei": "0", "ssc": "0" }
    """