@api @verifyCredentials
Feature: As an api a user
  I must be able to verify given credentials

  @Verify @fail
  Scenario: Verify given credentials without token in request body
    When send post request to "http://127.0.0.1:8081/accounts/verify/connectionIdNonExists1", with data
    """
    {
      "token": ""
    }
    """
    Then JSON node "error" should have data
    """
      "no JWT passed into decodeJWT"
    """

  @Verify @fail
  Scenario: Verify given credentials with incorrect token in request body
    When send post request to "http://127.0.0.1:8081/accounts/verify/1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed", with data
    """
    {
      "token": "eyJ0eX"
    }
    """
    Then JSON node "error" should have data
    """
      "Incorrect format JWT"
    """

  @Verify @fail
  Scenario: Verify given credentials with correct and expired token in request body
    When send post request to "http://127.0.0.1:8081/accounts/verify/1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed", with data
    """
    {
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NkstUiJ9.eyJpYXQiOjE1Nzc5NzE2MzgsImV4cCI6MTU3Nzk3MjIzOCwicmVxdWVzdGVkIjpbIm5hbWUiLCJwaG9uZSIsImlkZW50aXR5X25vIl0sImNhbGxiYWNrIjoiaHR0cDovLzEyNy4wLjAuMTo4MDgxLyIsInR5cGUiOiJzaGFyZVJlcSIsImlzcyI6ImRpZDpldGhyOjB4ZWI4MTJlYjcyZGQyMmIwNmVjMTRkMGM0ZmQ2ZWZmZjBkOTA4YjFlNyJ9.eustKtzglcYOi5lOBpE0zy37RLmezO5uj_wtBSnebEmJRCM9JjGstk9BQf968oHNi0CldYQa2UQsA8vEaoS1bAE"
    }
    """
    Then JSON node "error" should exist