// Inject socketIo directly
const socketIo = function(
    ioFunction,
    ioRedisFunction,
    credentials,
    qrCode,
    port = 8083
) {
    function attachListeners() {
        if ('function' !== typeof ioFunction && 'Server' !== ioFunction.name) {
            throw new Error('io is not a function');
        }

        if ('string' !== typeof credentials.did) {
            throw new Error(`Did not present in credentials`);
        }

        const io = ioFunction(port);
        const ioRedis = io.adapter(ioRedisFunction({
            host: (process.env.REDIS_HOST || 'redis')
        }));

        ioRedis.joinDisclosure = (redisInstance, socketId) => {
            redisInstance.of('/').adapter.remoteJoin(socketId, 'disclosure', (err) => {
                if (err) {
                    console.log(`[IO_REDIS] error: ${err.message}`);
                }
            });
        };

        ioRedis.on('connection', async function(socket) {
            ioRedis.joinDisclosure(ioRedis, socket.id);

            socket.on('createDisclosureRequest', async() => {
                try {
                    const callbackBase = (process.env.CALLBACK_URL || `http://localhost:${port}`);
                    const connectionId = socket.id;
                    const callbackUrl = `${callbackBase}/accounts/verify/${connectionId}`;
                    const params = {
                        requested: ['name', 'phone', 'identity_no'],
                        callbackUrl: new URL(callbackUrl).toString(),
                    };

                    const disclosureRequest = await credentials.createDisclosureRequest(params);
                    const qrCodeBody = await qrCode.toDataURL(disclosureRequest);

                    // Consider parsing qr code
                    const response = {
                        disclosureRequest: disclosureRequest,
                        qrCode: qrCodeBody,
                        params: params
                    };

                    socket.emit('disclosureRequestPayload', JSON.stringify(response));
                } catch (error) {
                    socket.emit('disclosureRequestError', JSON.stringify({ error: error.message }));
                }
            });
        });

        return ioRedis;
    }

    return {
        attachListeners
    }
};

module.exports = {
    socketIo
};
